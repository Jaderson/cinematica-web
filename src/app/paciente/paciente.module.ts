import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PacienteRoutingModule } from './paciente-routing.module';
import { PacienteComponent } from './paciente.component';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { DataListComponent } from '../data-list/data-list.component';


@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PacienteRoutingModule),
      SharedModule
  ],
  declarations: [PacienteComponent, DataListComponent]
})
export class PacienteModule { }
