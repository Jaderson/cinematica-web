import { Injectable } from "@angular/core";
import { Profissao } from "../profissao/profissao.dto";
import { Medico } from "app/medico/medico.dto";

@Injectable()
export class Paciente {

  private id: number ;
	private nome: string;
	private nomeCompleto: string;
	private criadoEm: Date;
	private atualizadoEm: Date = new Date();
	private tipoPessoa: string;

	private empresa: number;
	private cpf: string;
	private rg: string;
	private sexo: string;
	/*private Endereco endereco = new Endereco();*/
	private email: string;
	private telefoneCelular: string;
	private fotoUrl: string;
	private numeroCalcado: number;
	private profissao: Profissao;
	private funcionario: string;
	private dataNascimento: string;
	private crefito: string;
	private telefone: string;
	private fotoUrlEndereco: string;
	private indicacao: string;
	private medico: Medico;

}
