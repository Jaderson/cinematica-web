import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PacienteComponent } from './paciente.component';

export const PacienteRoutingModule: Routes = [{
  path: '',
  component: PacienteComponent,
  data: {
    breadcrumb: "Paciente"
  }
}];
