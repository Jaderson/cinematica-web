import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fadeInOutTranslate } from '../shared/elements/animation';
import { Paciente } from './paciente.dto';
import { GenericService } from 'app/generic-service/generic-service';
import { ErrorHandlerService } from 'app/util/error-handler.service';
import { Http } from '@angular/http';
import { Medico } from 'app/medico/medico.dto';
import { IOption } from 'ng-select';
import { FormGroup, FormControl } from '@angular/forms';
import { DataListComponent } from '../data-list/data-list.component';

@Component({
  selector: 'app-paciente',
  templateUrl: './paciente.component.html',
  styleUrls: ['./paciente.component.css',
    '../../assets/icon/SVG-animated/svg-weather.css'
  ],
  animations: [fadeInOutTranslate],
  encapsulation: ViewEncapsulation.None
})
export class PacienteComponent implements OnInit {

  public rowsFilter: Array<Paciente> = new Array<Paciente>();
  public entity = new Paciente();
  public medico = new Medico();
  public selected = [];
  simpleOption: Array<IOption> = new Array<IOption>();
  mySelectValue: Array<string>;
  public isDisableCadastrar: boolean;
  public myForm: FormGroup;
  public service: GenericService<Paciente>;
  public columns: any[];
  @ViewChild(DataListComponent) dataTable: DataListComponent<Paciente>;
  editing = {};
  showDialog: boolean = false;
  visibleCadastrar: boolean = false;
  enableButton: boolean = false;
  visibleDetalhes: boolean = false;
  defaultDateSelected: Date = new Date();

  constructor(public _servicePaciente: GenericService<Paciente>, public _errorHandler: ErrorHandlerService, public _serviceMedico: GenericService<Medico>) {
    this._servicePaciente.requestMap = '/paciente';
    this.service = this._servicePaciente;
  }

  ngOnInit() {
    this.columns = [
      { name: 'Nome', prop: 'nome'},
      { name: 'Telefone', prop: 'telefoneCelular '}
    ]
  }

  novo() {
    this.visibleCadastrar = true;
    this.selected = [];
    this.enableButton = false;
    this.entity = new Paciente();
    this.mySelectValue = [];
    this.isDisableCadastrar = false;
  }

  consultAll() {
    this.dataTable.ngOnInit();
  }

  updateFilter(event) {
    this.dataTable.updateFilter(event);
  }

  onActivate(event) { }

  public setEntity(event){
    if(event != null){
      delete event[0].campoOrderBy;
      delete event[0].$$index;
      this.entity = event[0];
      this.mySelectValue = [String(event[0].especialidade.id), event[0].especialidade.descricao];
      this.enableButton = true;
    }
  }

  create(form: FormControl) {
    this._servicePaciente.requestMap = '/paciente';
    this._servicePaciente.create(this.entity).then(() => {
      this._errorHandler.showNotification({ title: 'Sucesso', msg: 'Cadastrado com sucesso', timeout: 5000, theme: 'default', position: 'bottom-right', type: 'success' });
      this.consultAll();
      this.visibleCadastrar = false;
      this.isDisableCadastrar = false;
    }).catch(erro => {
      this._errorHandler.handle(erro);
    });
  }


}
