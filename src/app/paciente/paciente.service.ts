import { Injectable } from '@angular/core';
import { GenericService } from 'app/generic-service/generic-service';
import { ErrorHandlerService } from 'app/util/error-handler.service';
import { AuthHttp } from 'angular2-jwt';
import { Paciente } from './paciente.dto';

@Injectable()
export class PacienteService extends GenericService<Paciente> {

  constructor(public _http: AuthHttp, public _errorHandler: ErrorHandlerService) {
    super(_http, _errorHandler);
  }

}
