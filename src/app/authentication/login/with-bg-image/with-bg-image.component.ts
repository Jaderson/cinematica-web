import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { AuthService } from '../auth.service';
import { ErrorHandlerService } from 'app/util/error-handler.service';

@Component({
  selector: 'app-with-bg-image',
  templateUrl: './with-bg-image.component.html'
})
export class WithBgImageComponent implements OnInit {

  constructor(
    private _http: Http,
    private _auth: AuthService,
    private _erroHandler: ErrorHandlerService,
  ) { }

  ngOnInit() {
    this.login('admin', 'morais');
  }

  login(usuario: string, senha: string) {
    this._auth.login(usuario, senha)
      .then((response) => {
      })
      .catch( error => {
        this._erroHandler.handle(error);
      });
  }
}
