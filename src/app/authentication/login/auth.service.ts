import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { JwtHelper } from 'angular2-jwt';


@Injectable()
export class AuthService {
  jwtPayload: any;

  oauthTokenUrl = 'http://localhost:8080/oauth/token';
    constructor(private _http: Http, private _jwtHelper: JwtHelper) {
      this.carregarToken();
     }

    login(usuario: string, senha: string): Promise<void> {
      const headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      headers.append('Authorization', 'Basic Y2luZW1hdGljYTpjaW5lbWF0aWNh');
      const body = `username=${usuario}&password=${senha}&grant_type=password`;
      return this._http.post(this.oauthTokenUrl, body, {headers})
        .toPromise()
        .then(response => {
          console.log(response);
          this.armazenarToken(response.json().access_token);
        })
        .catch(response => {
          if ( response.status === 400) {
            const responseJson = response.json();

            if ( responseJson.error === 'invalid_grant') {
              return Promise.reject('Usuário ou senha inválida');
            }
          }
          return Promise.reject(response);
        });
      }

    private armazenarToken(token: string) {
      this.jwtPayload = this._jwtHelper.decodeToken(token);
      localStorage.setItem('token', token);
    }

    private carregarToken() {
      const token = localStorage.getItem('token');

      if (token) {
        this.armazenarToken(token);
      }
    }
}
