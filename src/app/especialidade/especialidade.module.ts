import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EspecialidadeRoutingModule } from './especialidade-routing.module';
import { EspecialidadeComponent } from './especialidade.component';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { DataListComponent } from 'app/data-list/data-list.component';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(EspecialidadeRoutingModule),
      SharedModule
  ],
  declarations: [EspecialidadeComponent,DataListComponent]
})
export class EspecialidadeModule { }
