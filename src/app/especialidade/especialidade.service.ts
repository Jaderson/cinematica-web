import { Injectable } from '@angular/core';
import { NgForm, FormControl } from '@angular/forms';
import { Especialidade } from 'app/especialidade/especialidade.dto';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { ErrorHandlerService } from 'app/util/error-handler.service';
import { GenericService } from '../generic-service/generic-service';

@Injectable()
export class EspecialidadeService extends GenericService<Especialidade> {

  constructor(public _http: AuthHttp, public _errorHandler: ErrorHandlerService) {
    super(_http, _errorHandler);
  }

}
