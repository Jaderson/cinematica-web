import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EspecialidadeComponent } from 'app/especialidade/especialidade.component';

export const EspecialidadeRoutingModule: Routes = [{
  path: '',
  component: EspecialidadeComponent,
  data: {
    breadcrumb: "Especialidade"
  }
}];

