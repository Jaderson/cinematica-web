import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Especialidade } from 'app/especialidade/especialidade.dto';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { EspecialidadeService } from 'app/especialidade/especialidade.service';
import { ErrorHandlerService } from 'app/util/error-handler.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { CustomValidators } from 'ng2-validation';
import { ToastyService, ToastOptions, ToastData } from "ng2-toasty";
import { fadeInOutTranslate } from '../shared/elements/animation';
import { GenericService } from '../generic-service/generic-service';
import { DataListComponent } from '../data-list/data-list.component';
import { GenericEntity } from '../generic-service/generic-entity';


@Component({
  selector: 'app-especialidade',
  templateUrl: './especialidade.component.html',
  styleUrls: ['./especialidade.component.css',
    '../../assets/icon/SVG-animated/svg-weather.css'
  ],
  animations: [fadeInOutTranslate],
  encapsulation: ViewEncapsulation.None
})
export class EspecialidadeComponent implements OnInit{

  public entity = new Especialidade();
  public myForm: FormGroup;
  public service: GenericService<Especialidade>;
  public columns: any[];
  @ViewChild(DataListComponent) dataTable: DataListComponent<Especialidade>;
  editing = {};
  showDialog: boolean = false;
  visibleCadastrar: boolean = false;
  enableButton: boolean = false;
  editProfile: boolean = true;
  editProfileIcon: string = 'icofont-edit';
  visibleDetalhes: boolean = false;
  visibleEditToggle: boolean = true;
  toolTipEdit: string = 'Editar';


  constructor(public _serviceEspecialidade: GenericService<Especialidade>, public _errorHandler: ErrorHandlerService) {
    this._serviceEspecialidade.requestMap = '/especialidade';
    this.service = this._serviceEspecialidade;
  }

  ngOnInit() {
    this.columns = [{
       name: 'Descrição', prop: 'descricao'
    }]
  }

  novo() {
    this.entity = new Especialidade();
    this.visibleCadastrar = true;
    this.dataTable.selected = [];
    this.enableButton = false;
  }

  close(event) {
    this.visibleCadastrar = false;
  }

  create(form: FormControl) {
    this._serviceEspecialidade.create(this.entity).then(() => {
      this._errorHandler.showNotification({ title: 'Sucesso', msg: 'Cadastrado com sucesso', timeout: 5000, theme: 'default', position: 'bottom-right', type: 'success' });
      this.consultAll();
      this.visibleCadastrar = false;
      this.dataTable.selected = [];
    }).catch(erro => {
      this._errorHandler.handle(erro);
    });
  }

  details() {
    this.visibleDetalhes = true;
    this.visibleEditToggle = true;
    this.toolTipEdit = 'Editar';
  }

  consultAll() {
    this.dataTable.ngOnInit();
  }

  updateCell() {
    this._serviceEspecialidade.update(this.entity).then(() => {
      this._errorHandler.showNotification({ title: 'Sucesso', msg: 'Atualizado com sucesso', timeout: 5000, theme: 'default', position: 'bottom-right', type: 'success' });
      this.consultAll();
    }).catch(erro => {
      this._errorHandler.handle(erro);
    });
  }

  updateDetails(form: FormControl) {
    this._serviceEspecialidade.update(this.entity).then(() => {
      this._errorHandler.showNotification({ title: 'Sucesso', msg: 'Atualizado com sucesso', timeout: 5000, theme: 'default', position: 'bottom-right', type: 'success' });
      this.consultAll();
      this.visibleCadastrar = false;
      this.visibleDetalhes = false;
      this.dataTable.selected = [];
      this.enableButton = false;
      this.toggleCancel();
    }).catch(erro => {
      this._errorHandler.handle(erro);
    });
  }

  delete() {
    this._serviceEspecialidade.delete(this.entity.id).then(() => {
      this._errorHandler.showNotification({ title: 'Sucesso', msg: 'Registro excluído com sucesso', timeout: 5000, theme: 'default', position: 'bottom-right', type: 'success' });
      this.visibleCadastrar = false;
      this.visibleDetalhes = false;
      this.dataTable.selected = [];
      this.enableButton = false;
      this.consultAll();
    }).catch(erro => {
      this._errorHandler.handle(erro);
    });
  }

  updateFilter(event) {
    this.dataTable.updateFilter(event);
  }

  onActivate(event) { }

  toggleEdit() {
    this.editProfileIcon = (this.editProfileIcon === 'icofont-ui-close') ? 'icofont-edit' : 'icofont-ui-close';
    this.editProfile = !this.editProfile;
    if (this.editProfileIcon === 'icofont-ui-close') {
      this.visibleEditToggle = false;
      this.toolTipEdit = 'Fechar';
    } else {
      this.visibleEditToggle = true;
      this.toolTipEdit = 'Editar';
    }
  }

  toggleCancel() {
    this.editProfileIcon = 'icofont-edit';
    if (this.editProfile == false) {
      this.editProfile = !this.editProfile;
    }
    this.visibleEditToggle = false;
    this.enableButton = false;
    this.visibleDetalhes = false;
    this.dataTable.selected = [];
  }

  public setEntity(event){
    if(event != null){
      delete event[0].campoOrderBy;
      delete event[0].$$index;
      this.entity = event[0];
      this.enableButton = true;
    }
  }

}
