import { Injectable } from "@angular/core";

@Injectable()
export class Especialidade {
    id : number;
    descricao: string;
}
