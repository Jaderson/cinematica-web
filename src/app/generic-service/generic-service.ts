import { AuthHttp } from "angular2-jwt";
import { ErrorHandlerService } from "../util/error-handler.service";
import { IOption } from "ng-select";
import { IGenericService } from "./i-generic-service";
import { Injectable } from "@angular/core";

@Injectable()
export class GenericService<T> implements IGenericService<T> {

  public url = 'http://www.cinematicaesportiva.com.br/fisio-web/api/';
  public urlLocal = 'http://localhost:8080';
  public dto: any;
  public requestMap: string;

  constructor(public _http: AuthHttp, public _errorHandler: ErrorHandlerService) {
    this._http = _http;
    this._errorHandler = _errorHandler;
  }

  create(object: T): Promise<T> {
    const url = this.urlLocal + this.requestMap;
    return this._http.post(url, object).toPromise()
    .then((response => response.json()));
  }

  update(object: T): Promise<T> {
    const url = this.urlLocal + this.requestMap;
    return this._http.put(url, object).toPromise()
    .then(response => response.json());
  }

  delete(id: number): Promise<void> {
    return this._http.delete(this.urlLocal + this.requestMap + '/' + id).toPromise()
    .then(() => null);
  }

  consultById(id: number): Promise<T> {
    return this._http.get(this.urlLocal + this.requestMap + '/' + id).toPromise()
    .then(response => response.json());
  }

  consultAll(): Promise<any> {
    return this._http.get(this.urlLocal + this.requestMap).toPromise()
    .then(response => response.json());
  }

  public cloneOptions(options: Array<any>): Array<IOption> {
    return options.map(option => ({ value: option.id, label: option.descricao }));
  }

  consultSelectAll(requestMap: string): Promise<any> {
    return this._http.get(this.urlLocal + requestMap).toPromise()
      .then(response => response.json());
  }
}
