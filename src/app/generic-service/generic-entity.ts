import { Component, OnInit } from '@angular/core';

export class GenericEntity<T> implements OnInit {

  public entity: T;

  constructor() { }

  ngOnInit() {
  }

  public setEntity(entidade: T): void {
		this.entity = entidade;
  }

  public getEntity(): T {
    return this.entity;
  }

}
