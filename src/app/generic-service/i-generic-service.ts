export interface IGenericService<T> {

  requestMap: string;
  url: string;
  urlLocal: string ;

  create(object: T): Promise<T>;
  update(object: T): Promise<T>;
  delete(id: number): Promise<void>;
  consultById(id: number): Promise<T>;
  consultAll(): Promise<any>;
  consultSelectAll(requestMap: string): Promise<any>;

}
