import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fadeInOutTranslate } from '../shared/elements/animation';
import { MedicoService } from './medico.service';
import { ErrorHandlerService } from '../util/error-handler.service';
import { Medico } from './medico.dto';
import { SelectOptionService } from 'app/shared/elements/select-option.service';
import { IOption } from 'ng-select';
import { Subscription } from 'rxjs';
import { EspecialidadeService } from '../especialidade/especialidade.service';
import { Especialidade } from '../especialidade/especialidade.dto';
import { GenericService } from '../generic-service/generic-service';
import { FormControl, FormGroup } from '@angular/forms';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { DataListComponent } from 'app/data-list/data-list.component';

@Component({
  selector: 'app-medico',
  templateUrl: './medico.component.html',
  styleUrls: ['./medico.component.css',
    '../../assets/icon/SVG-animated/svg-weather.css'
  ],
  animations: [fadeInOutTranslate],
  encapsulation: ViewEncapsulation.None
})
export class MedicoComponent implements OnInit {

  public rowsFilter: Array<Medico> = new Array<Medico>();
  public entity = new Medico();
  public especialidade = new Especialidade();
  public selected = [];
  simpleOption: Array<IOption> = new Array<IOption>();
  mySelectValue: Array<string>;
  public isDisableCadastrar: boolean;
  public myForm: FormGroup;
  public service: GenericService<Medico>;
  public columns: any[];
  @ViewChild(DataListComponent) dataTable: DataListComponent<Medico>;
  editing = {};
  showDialog: boolean = false;
  visibleCadastrar: boolean = false;
  enableButton: boolean = false;
  editProfile: boolean = true;
  editProfileIcon: string = 'icofont-edit';
  visibleDetalhes: boolean = false;
  visibleEditToggle: boolean = true;
  toolTipEdit: string = 'Editar';

  constructor(public _serviceMedico: GenericService<Medico>, public _errorHandler: ErrorHandlerService, public _serviceEspecialidade: GenericService<Especialidade>) {
    this._serviceMedico.requestMap = '/medico';
    this.service = this._serviceMedico;
  }

  ngOnInit() {
    this.consultComboBox();
    this.columns = [
      { name: 'Nome', prop: 'nome'},
      { name: 'E-mail', prop: 'email'},
      { name: 'Telefone', prop: 'telefone'},
      { name: 'Especialidade', prop: 'especialidade.descricao' }
    ]
  }

  novo() {
    this.visibleCadastrar = true;
    this.selected = [];
    this.enableButton = false;
    this.entity = new Medico();
    this.mySelectValue = [];
    this.isDisableCadastrar = false;
  }

  close(event) {
    this.visibleCadastrar = false;
  }

  create(form: FormControl) {
    this._serviceMedico.requestMap = '/medico';
    this._serviceMedico.create(this.entity).then(() => {
      this._errorHandler.showNotification({ title: 'Sucesso', msg: 'Cadastrado com sucesso', timeout: 5000, theme: 'default', position: 'bottom-right', type: 'success' });
      this.consultAll();
      this.visibleCadastrar = false;
      this.isDisableCadastrar = false;
    }).catch(erro => {
      this._errorHandler.handle(erro);
    });
  }

  details() {
    this.visibleDetalhes = true;
    this.visibleEditToggle = true;
    this.toolTipEdit = 'Editar';
  }

  consultAll() {
    this.dataTable.ngOnInit();
  }

  updateCell() {
    this._serviceMedico.update(this.entity).then(() => {
      this._errorHandler.showNotification({ title: 'Sucesso', msg: 'Atualizado com sucesso', timeout: 5000, theme: 'default', position: 'bottom-right', type: 'success' });
      this.consultAll();
    }).catch(erro => {
      this._errorHandler.handle(erro);
    });
  }

  updateDetails(form: FormControl) {
    this._serviceMedico.requestMap = '/medico'
    this._serviceMedico.update(this.entity).then(() => {
      this._errorHandler.showNotification({ title: 'Sucesso', msg: 'Atualizado com sucesso', timeout: 5000, theme: 'default', position: 'bottom-right', type: 'success' });
      this.consultAll();
      this.visibleCadastrar = false;
      this.visibleDetalhes = false;
      this.selected = [];
      this.enableButton = false;
      this.toggleCancel();
    }).catch(erro => {
      this._errorHandler.handle(erro);
    });
  }

  delete() {
    this._serviceMedico.delete(this.entity.id).then(() => {
      this._errorHandler.showNotification({ title: 'Sucesso', msg: 'Registro excluído com sucesso', timeout: 5000, theme: 'default', position: 'bottom-right', type: 'success' });
      this.consultAll();
      this.visibleCadastrar = false;
      this.visibleDetalhes = false;
      this.selected = [];
      this.enableButton = false;
    }).catch(erro => {
      this._errorHandler.handle(erro);
    });
  }

  updateFilter(event) {
    this.dataTable.updateFilter(event);
  }

  onActivate(event) { }

  toggleEdit() {
    this.editProfileIcon = (this.editProfileIcon === 'icofont-ui-close') ? 'icofont-edit' : 'icofont-ui-close';
    this.editProfile = !this.editProfile;
    if (this.editProfileIcon === 'icofont-ui-close') {
      this.visibleEditToggle = false;
      this.toolTipEdit = 'Fechar';
    } else {
      this.visibleEditToggle = true;
      this.toolTipEdit = 'Editar';
    }
  }

  toggleCancel() {
    this.editProfileIcon = 'icofont-edit';
    if (this.editProfile == false) {
      this.editProfile = !this.editProfile;
    }
    this.visibleEditToggle = false;
    this.enableButton = false;
    this.visibleDetalhes = false;
    this.selected = [];
  }

  onChange(event) {
    this._serviceEspecialidade.requestMap = '/especialidade'
    this._serviceEspecialidade.consultById(event).then(response => {
      let especialidade = new Especialidade();
      especialidade.id = response.id;
      especialidade.descricao = response.descricao;
      this.entity.especialidade = especialidade;
    });
  }

  consultComboBox() {
    this._serviceEspecialidade.consultSelectAll('/especialidade').then(response => {
      this.simpleOption = new Array<IOption>();
      response.results.forEach(element => {
        this.simpleOption.push({ value: String(element.id), label: element.descricao });
      });
    });
  }

  public setEntity(event){
    if(event != null){
      delete event[0].campoOrderBy;
      delete event[0].$$index;
      this.entity = event[0];
      this.mySelectValue = [String(event[0].especialidade.id), event[0].especialidade.descricao];
      this.enableButton = true;
    }
  }
}
