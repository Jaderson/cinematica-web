import { Especialidade } from "../especialidade/especialidade.dto";
import { Injectable } from "@angular/core";

@Injectable()
export class Medico {
    id : number;
    nome: string;
    telefone: string;
    email: string;
    especialidade: Especialidade = new Especialidade();
}
