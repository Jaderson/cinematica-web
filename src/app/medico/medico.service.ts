import { Injectable } from '@angular/core';
import { GenericService } from '../generic-service/generic-service';
import { AuthHttp } from 'angular2-jwt';
import { ErrorHandlerService } from '../util/error-handler.service';
import { Medico } from 'app/medico/medico.dto';
import { EspecialidadeService } from '../especialidade/especialidade.service';
import { Especialidade } from '../especialidade/especialidade.dto';
import { IOption } from 'ng-select';

@Injectable()
export class MedicoService extends GenericService<Medico> {

  constructor(public _http: AuthHttp, public _errorHandler: ErrorHandlerService) {
    super(_http, _errorHandler);
  }

}
