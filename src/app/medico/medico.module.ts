import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MedicoRoutingModule } from './medico-routing.module';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { MedicoComponent } from './medico.component';
import { DataListComponent } from '../data-list/data-list.component';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(MedicoRoutingModule),
      SharedModule
  ],
  declarations: [MedicoComponent, DataListComponent]
})
export class MedicoModule { }
