import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MedicoComponent } from './medico.component';

export const MedicoRoutingModule: Routes = [{
  path: '',
  component: MedicoComponent,
  data: {
    breadcrumb: "Medico"
  }
}];

