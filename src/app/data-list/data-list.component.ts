import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable/release/components';
import { GenericService } from '../generic-service/generic-service';
import { GenericEntity } from '../generic-service/generic-entity';

@Component({
  selector: 'app-data-list',
  templateUrl: './data-list.component.html',
  styleUrls: ['./data-list.component.css']
})
export class DataListComponent<T> implements OnInit {

  public rows: T[] = [];
  public rowsFilter = [];
  public selected = [];

  @ViewChild(DatatableComponent) table: DatatableComponent;
  @Input() public service: GenericService<T>;
  @Input() public columns: any[] = [];
  @Input() public columnsSearch: string;
  @Output() public entity: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
    this.service.consultAll().then(response => {
      this.rows = new Array<T>();
      if(typeof response.results != 'undefined'){

        for (let index = 0; index < response.results.length; index++) {
          const element = response.results[index];
          this.rows.push(element);
          this.rowsFilter.push(element);
         /*console.log(response);*/
        }
      } else{

        for (let index = 0; index < response.length; index++) {
          const element = response[index];
          this.rows.push(element);
          this.rowsFilter.push(element);
          /*console.log(response);*/
        }
      }
    });
  }

  public onSelect({selected}) {
    this.entity.emit(selected);
  }

  updateFilter(event) {

    let rowsPesquisa = [];
    let valorPesquisa = event.target.value.toLowerCase();

    let quantidadeColunasTabela = this.columns.length;
    let colunasTabela = Object.keys(this.rowsFilter[0]);

    rowsPesquisa = this.rowsFilter.filter(function (item) {
      for (let i = 0; i <= quantidadeColunasTabela; i++) {
        if (typeof item[colunasTabela[i]] == "string") {
          if ((<string>item[colunasTabela[i]]).toLowerCase().indexOf(valorPesquisa) !== -1 || !valorPesquisa) {
            return true;
          }
        }
      }
    });

    if (rowsPesquisa.length > 0) {
      this.rows = rowsPesquisa;
    } else {
      this.rows = [];
    }
  }

  onActivate(event) { }

}
