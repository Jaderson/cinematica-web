import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { DataListComponent } from 'app/data-list/data-list.component';
import { DatatableComponent } from '@swimlane/ngx-datatable/release/components/datatable.component';

@NgModule({
  imports: [
      CommonModule,
      SharedModule
  ],
  declarations: [DataListComponent]
})
export class DataListModule { }
