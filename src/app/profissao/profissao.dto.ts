import { Injectable } from "@angular/core";

@Injectable()
export class Profissao {
    id : number;
    descricao: string;
}
