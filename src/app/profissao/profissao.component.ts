import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fadeInOutTranslate } from '../shared/elements/animation';
import { Profissao } from './profissao.dto';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { FormGroup, FormControl } from '@angular/forms';
import { GenericService } from '../generic-service/generic-service';
import { ErrorHandlerService } from '../util/error-handler.service';
import { DataListComponent } from 'app/data-list/data-list.component';

@Component({
  selector: 'app-profissao',
  templateUrl: './profissao.component.html',
  styleUrls: ['./profissao.component.css',
    '../../assets/icon/SVG-animated/svg-weather.css'
  ],
  animations: [fadeInOutTranslate],
  encapsulation: ViewEncapsulation.None
})
export class ProfissaoComponent implements OnInit {

  public entity = new Profissao();
  public myForm: FormGroup;
  public service: GenericService<Profissao>;
  public columns: any[];
  @ViewChild(DataListComponent) dataTable: DataListComponent<Profissao>;
  editing = {};
  showDialog: boolean = false;
  visibleCadastrar: boolean = false;
  enableButton: boolean = false;
  editProfile: boolean = true;
  editProfileIcon: string = 'icofont-edit';
  visibleDetalhes: boolean = false;
  visibleEditToggle: boolean = true;
  toolTipEdit: string = 'Editar';


  constructor(public _serviceProfissao: GenericService<Profissao>, public _errorHandler: ErrorHandlerService) {
    this._serviceProfissao.requestMap = '/profissao';
    this.service = this._serviceProfissao;
  }

  ngOnInit() {
    this.columns = [{
      name: 'Descrição', prop: 'descricao'
   }]
  }

  novo() {
    this.entity = new Profissao();
    this.visibleCadastrar = true;
    this.dataTable.selected = [];
    this.enableButton = false;
  }

  close(event) {
    this.visibleCadastrar = false;
  }

  create(form: FormControl) {
    this._serviceProfissao.create(this.entity).then(() => {
      this._errorHandler.showNotification({ title: 'Sucesso', msg: 'Cadastrado com sucesso', timeout: 5000, theme: 'default', position: 'bottom-right', type: 'success' });
      this.consultAll();
      this.visibleCadastrar = false;
    }).catch(erro => {
      this._errorHandler.handle(erro);
    });
  }

  details() {
    this.visibleDetalhes = true;
    this.visibleEditToggle = true;
    this.toolTipEdit = 'Editar';
  }

  consultAll() {
   this.dataTable.ngOnInit();
  }

  updateCell() {
    this._serviceProfissao.update(this.entity).then(() => {
      this._errorHandler.showNotification({ title: 'Sucesso', msg: 'Atualizado com sucesso', timeout: 5000, theme: 'default', position: 'bottom-right', type: 'success' });
      this.consultAll();
    }).catch(erro => {
      this._errorHandler.handle(erro);
    });
  }

  updateDetails(form: FormControl) {
    this._serviceProfissao.update(this.entity).then(() => {
      this._errorHandler.showNotification({ title: 'Sucesso', msg: 'Atualizado com sucesso', timeout: 5000, theme: 'default', position: 'bottom-right', type: 'success' });
      this.consultAll();
      this.visibleCadastrar = false;
      this.visibleDetalhes = false;
      this.dataTable.selected = [];
      this.enableButton = false;
      this.toggleCancel();
    }).catch(erro => {
      this._errorHandler.handle(erro);
    });
  }

  delete() {
    this._serviceProfissao.delete(this.entity.id).then(() => {
      this._errorHandler.showNotification({ title: 'Sucesso', msg: 'Registro excluído com sucesso', timeout: 5000, theme: 'default', position: 'bottom-right', type: 'success' });
      this.consultAll();
      this.visibleCadastrar = false;
      this.visibleDetalhes = false;
      this.dataTable.selected = [];
      this.enableButton = false;
    }).catch(erro => {
      this._errorHandler.handle(erro);
    });
  }

  updateFilter(event) {
    this.dataTable.updateFilter(event);
  }

  onActivate(event) { }

  toggleEdit() {
    this.editProfileIcon = (this.editProfileIcon === 'icofont-ui-close') ? 'icofont-edit' : 'icofont-ui-close';
    this.editProfile = !this.editProfile;
    if (this.editProfileIcon === 'icofont-ui-close') {
      this.visibleEditToggle = false;
      this.toolTipEdit = 'Fechar';
    } else {
      this.visibleEditToggle = true;
      this.toolTipEdit = 'Editar';
    }
  }

  toggleCancel() {
    this.editProfileIcon = 'icofont-edit';
    if (this.editProfile == false) {
      this.editProfile = !this.editProfile;
    }
    this.visibleEditToggle = false;
    this.enableButton = false;
    this.visibleDetalhes = false;
    this.dataTable.selected = [];
  }

  public setEntity(event){
    if(event != null){
      delete event[0].campoOrderBy;
      delete event[0].$$index;
      this.entity = event[0];
      this.enableButton = true;
    }
  }

}
