import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfissaoRoutingModule } from './profissao-routing.module';
import { ProfissaoComponent } from './profissao.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { DataListComponent } from '../data-list/data-list.component';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(ProfissaoRoutingModule),
      SharedModule
  ],
  declarations: [ProfissaoComponent,DataListComponent]
})
export class ProfissaoModule { }
