import { Injectable } from '@angular/core';
import { Profissao } from './profissao.dto';
import { GenericService } from '../generic-service/generic-service';
import { AuthHttp } from 'angular2-jwt';
import { ErrorHandlerService } from '../util/error-handler.service';

@Injectable()
export class ProfissaoService extends GenericService<Profissao> {

  constructor(public _http: AuthHttp, public _errorHandler: ErrorHandlerService) {
    super(_http, _errorHandler);
  }

}
