import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfissaoComponent } from './profissao.component';

export const ProfissaoRoutingModule: Routes = [{
  path: '',
  component: ProfissaoComponent,
  data: {
    breadcrumb: "Profissão"
  }
}];
